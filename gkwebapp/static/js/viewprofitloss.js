$(document).ready(function() {
  $('.modal-backdrop').remove();
  $("#pnl_todate").focus();
  $('.pnl_autotab').autotab('number');
  var financialstart = Date.parseExact(sessionStorage.yyyymmddyear1, "yyyy-MM-dd");
  var financialend = Date.parseExact(sessionStorage.yyyymmddyear2, "yyyy-MM-dd");
  function pad (str, max) { //to add leading zeros in date
    str = str.toString();
    if (str.length==1) {
      return str.length < max ? pad("0" + str, max) : str;
    }
    else{
      return str
    }
  }
  function yearpad (str, max) {
    str = str.toString();
    if (str.length==1) {
      return str.length < max ? pad("200" + str, max) : str;
    }
    else if (str.length==2) {
      return str.length < max ? pad("20" + str, max) : str;
    }
    else{
      return str
    }
  }

  $("#pnl_todate").blur(function(event) {
    $(this).val(pad($(this).val(),2));
  });
  $("#pnl_tomonth").blur(function(event) {
    $(this).val(pad($(this).val(),2));
  });
  $("#pnl_toyear").blur(function(event) {
    $(this).val(yearpad($(this).val(),4));
  });

  $('input:text:enabled').keydown( function(e) {
    var n = $("input:text:enabled").length;
    var f = $('input:text:enabled');
      if (e.which == 13)
      {
        var nextIndex = f.index(this) + 1;
        if(nextIndex < n){
          e.preventDefault();
          f[nextIndex].focus();
          f[nextIndex].select();
        }
      }

      if (e.which == 38)
      {
        var prevIndex = f.index(this) - 1;
        if(prevIndex < n){
          e.preventDefault();
          f[prevIndex].focus();
          f[prevIndex].select();
        }
      }
    });

  var fromdatearray = sessionStorage.yyyymmddyear1.split(/\s*\-\s*/g)
  $("#pnl_fromdate").val(fromdatearray[2])
  $("#pnl_frommonth").val(fromdatearray[1])
  $("#pnl_fromyear").val(fromdatearray[0])
  var todatearray = sessionStorage.yyyymmddyear2.split(/\s*\-\s*/g)
  $("#pnl_todate").val(todatearray[2])
  $("#pnl_tomonth").val(todatearray[1])
  $("#pnl_toyear").val(todatearray[0])
  $("#pnl_todate").select();

  $("#pnl_toyear").keydown(function(event) {
    if (event.which==13) {
      $(this).val(yearpad($(this).val(),4));
      $("#pnl_view").click();
    }
  });

  $("#pnl_view").click(function(event) {
    var todate = $("#pnl_toyear").val()+$("#pnl_tomonth").val()+$("#pnl_todate").val();
    if(!Date.parseExact(todate, "yyyyMMdd")){
      $("#date-alert").alert();
      $("#date-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#date-alert").hide();
      });
      $('#pnl_todate').focus().select();
      return false;
    }

    if (!Date.parseExact(todate,"yyyyMMdd").between(financialstart,financialend)) {
      $("#between-date-alert").alert();
      $("#between-date-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#between-date-alert").hide();
      });
      $('#pnl_todate').focus().select();
      return false;
    }
    $.ajax(
      {
        type: "POST",
        url: "/showprofitlossreport",
        global: false,
        async: false,
        datatype: "text/html",
        data: {"financialstart":sessionStorage.yyyymmddyear1,"orgtype":sessionStorage.orgt,"calculateto":$("#pnl_toyear").val()+"-"+$("#pnl_tomonth").val()+"-"+$("#pnl_todate").val()},
        beforeSend: function(xhr)
        {
          xhr.setRequestHeader('gktoken',sessionStorage.gktoken );
        }
      })
      .done(function(resp) {
        $("#info").html(resp);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });

  });
  $("#pnl_reset").click(function(event) {
    $("#showprofitloss").click();
  });
});
